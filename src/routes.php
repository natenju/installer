<?php

/**
 * @author Tchapga Nana Hermas
 * @email hermasn@yahoo.fr
 * @create date 2018-12-18 12:07:43
 * @modify date 2018-12-18 12:07:43
 * @desc [description]
 */

Route::get(
    'install',
    ["uses" => 'Natenju\Installer\App\Http\Controllers\InstallerController@index', "as" => "installer.index"]
);
