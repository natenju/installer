<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>

    <title>{{ env('APP_NAME', "Installer") }}</title>

{{--    TODO: Change links from /vendo/... to {{ asset('css/...') }}--}}
    <!-- LINEARICONS -->
    <link rel="stylesheet" href="/vendor/installer/fonts/linearicons/style.css ">

    <!-- MATERIAL DESIGN ICONIC FONT -->
    <link rel="stylesheet" href="vendor/installer/fonts/material-design-iconic-font/css/material-design-iconic-font.css">

    <!-- STYLE CSS -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="/vendor/installer/css/style.css ">
    {{--<link href="/vendor/installer/css/animate.css" rel="stylesheet" />--}}
    <script src="{{ asset('/vendor/installer/js/jquery.bootstrap.js') }}" type="text/javascript"></script>
    <link rel="stylesheet" href="{{ asset('vendor/installer/css/material-bootstrap-wizard.css') }}">
    <style>
        .wizard-card .choice .icon {
            overflow: hidden;
        }
    </style>

</head>

<body>
    <div class="wrapper">
        <div class="image-holder hidden-xs hidden-sm hidden-md">
            {{--<img src="images/form-wizard.jpg" alt="">--}}
        </div>
        <div class="form-content wizard-container">
            <form action="#">
                <div id="wizard">
                    <!-- SECTION 1 -->
                    <h4></h4>
                    <section class="wizard-card" data-color="purple">
                        <h3>Choose Language</h3>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <div class="choice" data-toggle="wizard-radio" rel="tooltip" title="Choisir cette
                                option pour continnuer en Français.">
                                    <input type="radio" name="type" value="fr">
                                    <div class="icon">
                                        <img src="/vendor/installer/images/fr.png" alt="français.png">
                                    </div>
                                    <h6>Français</h6>
                                </div>
                            </div>
                            <div class="col-md-2"></div>
                            <!-- /.col-md-2 -->
                            <div class="col-sm-4">
                                <div class="choice" data-toggle="wizard-radio" rel="tooltip" title="Select this
                                option to contnue in English.">
                                    <input type="radio" name="type" value="en">
                                    <div class="icon">
                                        <img src="/vendor/installer/images/en.png" alt="english.png">
                                    </div>
                                    <h6>Englsh</h6>
                                </div>
                            </div>
                        </div>
                    </section>

                    <!-- SECTION 2 -->
                    <h4></h4>
                    <section>
                        <h3>Step 2</h3>
                    </section>

                    <!-- SECTION 3 -->
                    <h4></h4>
                    <section>
                        <h3 style="margin-bottom: 50px;">Step 3</h3>

                    </section>
                    <!-- SECTION FINISH -->
                    <h4></h4>
                    <section>
                        <h3>Finish</h3>
                        <div class="info">
                            <div class="info-item">
                                <span class="lnr lnr-calendar-full"></span>
                                <span class="unit">Date:</span> August 1 @ 8:00 am
                            </div>
                            <div class="info-item">
                                <span class="lnr lnr-clock"></span>
                                <span class="unit">Time:</span> 8:00 am - 5:00 pm
                            </div>
                            <div class="info-item">
                                <span class="lnr lnr-apartment"></span>
                                <span class="unit">Venue:</span> National Conference
                            </div>
                            <div class="info-item">
                                <span class="lnr lnr-map"></span>
                                <span class="unit">Address:</span> No 40 Baria Sreet 133/2
                            </div>
                            <div class="info-item">
                                <span class="lnr lnr-earth"></span>
                                <span class="unit">Website: </span> Info@yourwev.com
                            </div>
                        </div>
                    </section>
                </div>
            </form>
        </div>
    </div>

    <script src="/vendor/installer/js/jquery-3.3.1.min.js"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('/vendor/installer/js/jquery.bootstrap.js') }}"></script>
    <!-- JQUERY STEP -->
    <script src="/vendor/installer/js/jquery.steps.js"></script>

    <script src="/vendor/installer/js/main.js"></script>

    <script src="/vendor/installer/js/material-bootstrap-wizard.js"></script>
    <!--  More information about jquery.validate here: http://jqueryvalidation.org/	 -->
    <script src="/vendor/installer/js/jquery.validate.min.js"></script>
</body>

</html>
