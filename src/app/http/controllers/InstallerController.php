<?php
/**
 * @Author Tchapga Nana Hermas <[hermastchapga@gmail.com]>.
 * @Created: 3/11/2019 5:13 PM
 * @Updated: 3/11/2019 5:13 PM
 * @Desc   : [DESCRIPTION]
 */

namespace Natenju\Installer\App\Http\Controllers;


use App\Http\Controllers\Controller;

class InstallerController extends Controller {
    public function index() {
        return view('installer::wizard');
    }
}