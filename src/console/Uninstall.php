<?php

/**
 * @author Tchapga Nana Hermas
 * @email hermasn@yahoo.fr
 * @create date 2018-12-14 17:16:25
 * @modify date 2018-12-14 17:16:25
 * @desc [description]
 */
namespace Natenju\Installer\Console;

use Illuminate\Console\Command;

class Uninstall extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = "installer:uninstall";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Uninstall Your app using command line";

    /**
     * handle
     *
     * @return void
     */
    public function handle()
    {
        $this->warn("this will completely delete your app. continue");
        if ($this->confirm("continue?")) {
            #Code
        }
    }
}
