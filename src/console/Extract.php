<?php
namespace Natenju\Installer\Console;

use Illuminate\Console\Command;

class Extract extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = "installer:extract {parameters}";

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = "installer:extract";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Extract Your app in command line";

    /**
     * handle
     *
     * @return void
     */
    public function handle()
    {

        $this->header();

        $this->checkRequirements();

        $this->info("Launching...");

        if ($this->confirm('Do you have database configuration at .env ?')) {

            $this->info('Dumping the autoloaded files and reloading all new files...');
            $composer = $this->findComposer();
            $process = new Process($composer . ' dumpautoload');
            $process->setWorkingDirectory(base_path())->run();

            $this->info('Completed ! Thank You.');
        } else {
            $this->info('Setup Aborted !');
            $this->info('Please setting the database configuration for first !');
        }

        $this->footer();
    }

    private function header()
    {
        $this->info("--------- setting up " . env('APP_NAME', "App") . " ---------------");

    }

    /**
     * Checks that
     * laravel >=5.3
     * /publi/ is readable
     *
     * @return void
     */
    private function checkRequirements()
    {
        $this->info('System Requirements Checking:');
        $system_failed = 0;
        $laravel = app();

        if ($laravel::VERSION >= 5.3) {
            $this->info('Laravel Version (>= 5.3.*): [Good]');
        } else {
            $this->info('Laravel Version (>= 5.3.*): [Bad]');
            $system_failed++;
        }

        if (is_readable(base_path('public'))) {
            $this->info('public dir is writable: [Good]');
        } else {
            $this->info('public dir is writable: [Bad]');
            $system_failed++;
        }

        if ($system_failed != 0) {
            $this->info('Sorry unfortunately your system did not meet with our requirements !');
            $this->footer(false);
        }
    }

    /**
     * footer
     *
     * @param  mixed $success
     *
     * @return void
     */
    private function footer($success = true)
    {
        $this->info('====================================================================');
        if ($success == true) {
            $this->info('-------------------  Completed !!  ------------------------');
        } else {
            $this->info('-------------------    Failed !!   ------------------------');
        }
        exit;
    }
}
