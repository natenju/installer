<?php

/**
 * @author Tchapga Nana Hermas
 * @email hermasn@yahoo.fr
 * @create date 2018-12-18 12:04:51
 * @modify date 2018-12-18 12:04:51
 * @desc [description]
 */

namespace Natenju\Installer;

use Illuminate\Support\ServiceProvider;
use Natenju\Installer\Console\Extract;
use Natenju\Installer\Console\Launch;
use Natenju\Installer\Console\Uninstall;
use Natenju\Installer\Console\Update;

/**
 * Class InstallerServiceProvider
 *
 * @package Natenju\Installer
 */
class InstallerServiceProvider extends ServiceProvider {
    
    /**
     * Post-registration booting of services.
     *
     * @return void
     */
    public function boot() {
        $this->publishes(
            [
                __DIR__ . "/config/installer.php" => config_path("installer.php"),
            ],
            "installer"
        );
        
        $this->loadRoutesFrom(__DIR__ . "/routes.php");
        
        $this->loadMigrationsFrom(__DIR__ . '/database/migrations');
        
        $this->loadTranslationsFrom(__DIR__ . '/resources/lang/', 'installer');
        // $this->publishes(
        //     [
        //         __DIR__ . '/resources/lang/' => resource_path('lang/vendor/installer')
        //     ]
        // );
        
        $this->loadViewsFrom(__DIR__ . '/resources/views', 'installer');
        // $this->publishes(
        //     [
        //         __DIR__ . '/views' => 'views/vendor/installer',
        //     ]
        // );
        
        if ( $this->app->runningInConsole() ) {
            $this->commands(
                [
                    Extract::class,
                    Launch::class,
                    Uninstall::class,
                    Update::class,
                ]
            );
        }
        
        $this->publishes(
            [
                __DIR__ . "/assets" => public_path("vendor/installer"),
            ],
            "installer"
        );
    }
    
    /**
     * Register function
     *
     * @return void
     */
    public function register() {}
}
